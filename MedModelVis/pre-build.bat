@echo off
set libs=0
set e7z=0
set e7z32=0
set e7z64=0
if exist "./include" set libs=1
if exist "./lib_debug" set libs=1
if exist "C:\Program Files\7-Zip\7z.exe" (
	set e7z=1
	set e7z32=1
)
if exist "C:\Program Files(x86)\7-Zip\7z.exe" (
	set e7z=1
	set e7z64=1
)
if %libs% EQU 0 (
	if %e7z% EQU 1 (
		if %e7z32% EQU 1 (
			"C:\Program Files\7-Zip\7z.exe" x third-party.7z
		)
		if %e7z64% EQU 1 (
			"C:\Program Files(x86)\7-Zip\7z.exe" x third-party.7z
		)
	) else (
		echo pre-build.bat: ERROR: 7-zip is required to extract third party libraries.
	)
) else (
	echo pre-build.bat: Third party libraries were found.
)
