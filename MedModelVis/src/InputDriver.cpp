#include <iostream>
#include "InputDriver.hpp"
#include "Leap.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <set>

/*
	TODO:
	- low pass filtering/minimum displacement for update
	- user-defined origin
	- translation
	- rotation
	- movement sensitivity relative to zoom level
*/

InputDriver::InputDriver(
	int buffSize_,
	CoordinateSystem coordinateSystem_,
	InputMode inputMode_
) :
		coordinateSystem(coordinateSystem_),
		inputMode(inputMode_)
{
    pointableList.set_capacity(buffSize_);
	gestureList.set_capacity(buffSize_);
	prevPos = Leap::Vector();
	
	setDefaultPosition(
		GeodesicPosition(
			DEFAULT_LATITUDE,
			DEFAULT_LONGITUDE,
			DEFAULT_ALTITUDE
		)
	);

	origin = Leap::Vector(0,100,0);
	invertCamera = DEFAULT_INVERT_CAMERA;
	originWasInitialized = false;
}

bool InputDriver::putPointables(Leap::PointableList data) {

	boost::mutex::scoped_lock buffLock(mutex);
	boost::system_time const timeout = 
		boost::get_system_time() +
		boost::posix_time::milliseconds(TIMEOUT_MILLIS);

	/*
	if (!buffFull.timed_wait(buffLock, timeout, boost::bind(&InputDriver::hasRoom, this))) {
		std::cout << "InputDriver::put() timeout!" << std::endl;
		return false;
	}
	*/
	
	pointableList.push_back(data);
	buffEmpty.notify_one();
	return true;

}

bool InputDriver::putGestures(Leap::GestureList data) {

	boost::mutex::scoped_lock buffLock(mutex);
	boost::system_time const timeout = 
		boost::get_system_time() +
		boost::posix_time::milliseconds(TIMEOUT_MILLIS);

	/*
	if (!buffFull.timed_wait(buffLock, timeout, boost::bind(&InputDriver::hasRoom, this))) {
		std::cout << "InputDriver::put() timeout!" << std::endl;
		return false;
	}
	*/
	
	gestureList.push_back(data);
	buffEmpty.notify_one();
	return true;

}

bool InputDriver::getInput(InputMessage &input) {
	boost::mutex::scoped_lock buffLock(mutex);
	boost::system_time const timeout = 
		boost::get_system_time() +
		boost::posix_time::milliseconds(TIMEOUT_MILLIS);

	if (!buffEmpty.timed_wait(buffLock, timeout, boost::bind(&InputDriver::hasData, this))) {
		return false;
	}

	if (getInputMessage(input)) {
		pointableList.pop_front();
		if(gestureList.size() > 0)
		{
			gestureList.pop_front();
		}
		buffFull.notify_one();
		return true;
	}

	return false;
}

/*
CAMERA
pos[0] +:forward, -:backward
pos[1] +:up,      -:down
pos[2] +:right,   -:left

LEAP
pos[0] +:right,    -:left
pos[1] +:up,       -:down
pos[2] +:backward, -:forward
*/

// latitude: up/down
// longitude: left/right

// TODO: invert x and z when x (and z?) is negative
//		 (or maybe modify the up vector?)
// TODO: initialize origin when a second pointable is present

bool InputDriver::getInputMessage(InputMessage &input) {
	bool gestureAvailable = false;
	printFrameInfo(pointableList.front()[0].frame());
	
	input.target = CAMERA_POSITION;
	
	Leap::PointableList pointables = pointableList.front();
	if(gestureList.size() > 0)
	{
		Leap::GestureList gestures = gestureList.front();
		std::vector<Leap::Gesture> validGestures = getValidGestures(gestures);
		if (validGestures.size() != 0) {
			Leap::Gesture ges = validGestures[0];
			gestureAvailable = true;
		}
	}
	std::vector<Leap::Pointable> validPointables = getValidPointables(pointables);
	
	if (validPointables.size() == 0) {
		return false;
	}
	
	if (USE_USER_ORIGIN) {
		initOrigin(validPointables);
		if (!originWasInitialized) {
			return false;
		}
	}

	Leap::Vector pos = validPointables[0].tipPosition();
	
	Leap::Vector displacement;

	if (inputMode == POSITION) {
		if(gestureAvailable)
		{ 
			input.target = SLICE_GESTURE;
		}
		else {

			displacement = prevPos - pos;
			displacement *= DISPLACEMENT_AMPLIFICATION;
		}
		
	} 
	else if (inputMode == VELOCITY) {
		displacement = origin - pos;
		displacement *= VELOCITY_AMPLIFICATION;
	}

	if (invertCamera) {
		displacement *= -1;
	}

	if (coordinateSystem == CARTESIAN) {
		cartesianPosition.x -= displacement.z;
		cartesianPosition.y += displacement.y;
		cartesianPosition.z += displacement.x;
	} else if (coordinateSystem == GEODESIC) {		
		geodesicPosition.latitude  += displacement.y;
		geodesicPosition.longitude += displacement.x;
		geodesicPosition.altitude  += displacement.z;
	}

	prevPos = pos;
	syncCoordinates();
	enforceBoundaries();
	input.data = cartesianPosition;

	//printDebugInfo();

	return true;
}

void InputDriver::enforceBoundaries() {
	if (geodesicPosition.altitude < MIN_ALTITUDE) {
		geodesicPosition.altitude = MIN_ALTITUDE;
	} else if (geodesicPosition.altitude > MAX_ALTITUDE) {
		geodesicPosition.altitude = MAX_ALTITUDE;
	}
	syncCoordinates();
}

// TOO: replace these with C++11 lambdas
bool InputDriver::hasData() {
	return !pointableList.empty();
}

bool InputDriver::hasRoom() {
	return !pointableList.full();
}

Leap::Vector InputDriver::getPosition() {
	return cartesianPosition;
}

void InputDriver::toggleCoordinateSystem() {
	syncCoordinates();
	switch(coordinateSystem) {
	case CARTESIAN:
		coordinateSystem = GEODESIC;
		std::cout << "Coordinate system: GEODESIC" << std::endl;
		break;
	case GEODESIC:
		coordinateSystem = CARTESIAN;
		std::cout << "Coordinate system: CARTESIAN" << std::endl;
		break;
	}
}

InputDriver::GeodesicPosition InputDriver::cartToGeo(Leap::Vector cart) {
	GeodesicPosition geo;
	geo.longitude = atan(cart.y / cart.x);
	geo.altitude = sqrt(cart.x*cart.x + cart.y*cart.y + cart.z*cart.z);
	geo.latitude = atan(sqrt(cart.x*cart.x + cart.y*cart.y)/cart.z);
	return geo;
}

Leap::Vector InputDriver::geoToCart(GeodesicPosition geo) {
	Leap::Vector cart;
	double LAT = geo.latitude * M_PI / 180;
	double LON = geo.longitude * M_PI / 180;
	double R = geo.altitude;
	cart.x = -R * cos(LAT) * cos(LON);
	cart.y = R * sin(LAT);
	cart.z = R * cos(LAT) * sin(LON);
	return cart;
}

void InputDriver::syncCoordinates() {
	switch (coordinateSystem) {
	case CARTESIAN:
		// convert cartesian to geodesic
		geodesicPosition = cartToGeo(cartesianPosition);
		break;
	case GEODESIC:
		// convert geodesic to cartesian
		cartesianPosition = geoToCart(geodesicPosition);
		break;
	}
}

void InputDriver::toggleInvertCamera() {
	invertCamera = !invertCamera;
	std::cout << "Invert camera: " << std::boolalpha << invertCamera << std::endl;
}

std::vector<Leap::Pointable> InputDriver::getValidPointables(Leap::PointableList pointables) {
	std::vector<Leap::Pointable> validPointables;
	for (int i = 0; i < pointables.count(); ++i) {
		Leap::Pointable pointable = pointables[i];
		Leap::Vector position = pointable.tipPosition();
		if (abs(position.x) < origin.x + ACTIVE_REGION_SIZE &&
			abs(position.y) < origin.y + ACTIVE_REGION_SIZE &&
			abs(position.z) < origin.z + ACTIVE_REGION_SIZE) {
				validPointables.push_back(pointable);
		}
	}
	return validPointables;
}

std::vector<Leap::Gesture> InputDriver::getValidGestures(Leap::GestureList gestures) {
	std::vector<Leap::Gesture> validGestures;
	for (int i = 0; i < gestures.count(); ++i) {
		Leap::Gesture gesture = gestures[i];
		if (gesture.type() == Leap::Gesture::TYPE_SWIPE) 
		{
			validGestures.push_back(gesture);
		}
	}
	return validGestures;
}

void InputDriver::printDebugInfo() {
	std::cout	<< "Leap position: " 
				<< prevPos[0] << ","
				<< prevPos[1] << ","
				<< prevPos[2] << std::endl
				<< "Camera position (lat,lon,alt): "
				<< geodesicPosition.latitude << ","
				<< geodesicPosition.longitude << ","
				<< geodesicPosition.altitude << std::endl
				<< "Camera position (x,y,z): "
				<< cartesianPosition.x << ","
				<< cartesianPosition.y << ","
				<< cartesianPosition.z << std::endl
				<< std::endl;
}

void InputDriver::toggleInputMode() {
	switch(inputMode) {
	case POSITION:
		inputMode = VELOCITY;
		std::cout << "Input mode: VELOCITY" << std::endl;
		break;
	case VELOCITY:
		inputMode = POSITION;
		std::cout << "Input mode: POSITION" << std::endl;
		break;
	}
}

void InputDriver::resetPosition() {
	std::cout << "Resetting position to "
				<< defaultPosition.x << ","
				<< defaultPosition.y << ","
				<< defaultPosition.z << std::endl;
	cartesianPosition = defaultPosition;
	CoordinateSystem tmp = coordinateSystem;
	coordinateSystem = CARTESIAN;
	syncCoordinates();
	coordinateSystem = tmp;
}


void InputDriver::setDefaultPosition(Leap::Vector cart) {
	defaultPosition = cart;
}
void InputDriver::setDefaultPosition(GeodesicPosition geo) {
	setDefaultPosition(geoToCart(geo));
}

void InputDriver::setDefaultPosition(
	double a,
	double b,
	double c,
	CoordinateSystem coordinateSystem
) {
	if (coordinateSystem == GEODESIC) {
		defaultPosition = geoToCart(GeodesicPosition(a,b,c));
	} else if (coordinateSystem == CARTESIAN) {
		defaultPosition = Leap::Vector(a,b,c);
	}
	std::cout << "Default Position: "
				<< defaultPosition.x << ","
				<< defaultPosition.y << ","
				<< defaultPosition.z << std::endl;
}

void InputDriver::initOrigin(std::vector<Leap::Pointable> pointables) {
	if (pointables.size() < 2) {
		return;
	}
	// TODO: enforce minimum distance, or check for separate hands
	sortPointables(pointables);
	origin = pointables[1].tipPosition();
	originWasInitialized = true;
	std::cout << "Initializing origin to "
				<< origin.x << ","
				<< origin.y << ","
				<< origin.z << std::endl;
}

void InputDriver::sortPointables(std::vector<Leap::Pointable> pointables) {
	std::sort(pointables.begin(), pointables.end(), isFirstLeftOf);
}

bool InputDriver::isFirstLeftOf(Leap::Pointable a, Leap::Pointable b) {
	return a.tipPosition().x < b.tipPosition().x;
}

void InputDriver::printFrameInfo(Leap::Frame frame) {
	std::cout << "Frame " << frame.id() << ":" << std::endl;
	Leap::HandList hands = frame.hands();
	for (int i = 0; i < hands.count(); ++i) {
		Leap::Hand hand = hands[i];
		std::cout	<< "\t"
					<< "Hand " << hand.id() << ":" << std::endl;
		Leap::PointableList pointables = hand.pointables();
		for (int i = 0; i < pointables.count(); ++i) {
			Leap::Pointable pointable = pointables[i];
			std::cout	<< "\t\t"
						<< "Pointable " << pointable.id() << std::endl;

		}
	}
}