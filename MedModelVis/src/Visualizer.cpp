#include "Visualizer.hpp"
#include <boost/thread/thread.hpp>
#include "Leap.h"
#include "MedModelVis.hpp"
#include <iostream>
#include <conio.h>
#include <cmath>
#include <pcl/filters/passthrough.h>

Visualizer::Visualizer(
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_,
	InputDriver *inputDriver_
	) :
cloud(cloud_),
	inputDriver(inputDriver_) 
{
	display_cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>(*cloud.get()));
}

void Visualizer::init() {
	pclVisualizer = boost::shared_ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer ("3D Viewer"));
	pclVisualizer->setBackgroundColor (0, 0, 0);
	//pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZRGB> rgb_color(display_cloud, "intensity");
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_color(display_cloud);
	pclVisualizer->addPointCloud<pcl::PointXYZRGB>(display_cloud, rgb_color, "sample cloud");
	pclVisualizer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
	pclVisualizer->addCoordinateSystem(1.0);
	pclVisualizer->initCameraParameters();

	pclVisualizer->registerKeyboardCallback(boost::bind(&Visualizer::keyboardEventOccurred, this, _1));
	updateCameraPosition(inputDriver->getPosition());
}

void Visualizer::keyboardEventOccurred(const pcl::visualization::KeyboardEvent &e) {
	std::cout << "Keyboard event occurred: " << e.getKeySym() << std::endl;

	if (e.getKeySym() == "space" && e.keyDown()) {
		inputDriver->toggleCoordinateSystem();
	} else if (e.getKeySym() == "Return" && e.keyDown()) {
		inputDriver->toggleInvertCamera();
	} else if (e.getKeySym() == "Control_L" && e.keyDown()) {
		inputDriver->toggleInputMode();
	} else if (e.getKeySym() == "Alt_L" && e.keyDown()) {
		inputDriver->resetPosition();
		updateCameraPosition(inputDriver->getPosition());
	} else if (e.getKeySym()[0] == 'F' && e.keyDown()){
		std::cout << "Filtering" << std::endl;
		pcl::PassThrough<pcl::PointXYZRGB> pass;
		pass.setInputCloud (cloud);
		if (e.getKeySym() == "F1"){
			std::cout << "Reset" << std::endl;
			pass.setFilterFieldName ("z");
			pass.setFilterLimits (-1000.0, 1000.0);
		} else if (e.getKeySym() == "F3" && e.keyDown()){
			pass.setFilterFieldName ("x");
			pass.setFilterLimits (0.0, 1000.0);
		} else if (e.getKeySym() == "F4" && e.keyDown()){
			pass.setFilterFieldName ("x");
			pass.setFilterLimits (-1000.0, 0.0);
		} else if (e.getKeySym() == "F5" && e.keyDown()){
			pass.setFilterFieldName ("y");
			pass.setFilterLimits (0.0, 1000.0);
		} else if (e.getKeySym() == "F6" && e.keyDown()){
			pass.setFilterFieldName ("y");
			pass.setFilterLimits (-1000.0, 0.0);
		} else if (e.getKeySym() == "F7" && e.keyDown()){
			pass.setFilterFieldName ("z");
			pass.setFilterLimits (0.0, 1000.0);
		} else if (e.getKeySym() == "F8" && e.keyDown()){
			pass.setFilterFieldName ("z");
			pass.setFilterLimits (-1000.0, 0.0);
		}
		pass.filter (*display_cloud);
		pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_color(display_cloud);
		pclVisualizer->updatePointCloud<pcl::PointXYZRGB>(display_cloud, rgb_color, "sample cloud");
	}
}

void Visualizer::run() {
	// NOTE: "the thread creating the visualizer must be the same calling the spinOnce() method"
	// http://www.pcl-users.org/showRGBimage-hanging-if-not-on-main-thread-td3863701.html#a3888457
	init();

	InputDriver::InputMessage input;
	pcl::PassThrough<pcl::PointXYZRGB> pass;
	pass.setInputCloud (cloud);
	while (!pclVisualizer->wasStopped ()) {
		if (inputDriver->getInput(input)) {
			switch (input.target) {
			case InputDriver::CAMERA_POSITION:
				updateCameraPosition(input.data);
				break;
			case InputDriver::CAMERA_ORIENTATION:
				break;
			case InputDriver::CAMERA_FOCUS:
				break;
			case InputDriver::PLANE_POSITION:
				break;
			case InputDriver::PLANE_ORIENTATION:
				break;
			case InputDriver::SLICE_GESTURE:
				pass.setFilterFieldName ("x");
				pass.setFilterLimits (0.0, 1000.0);	
				pass.filter (*display_cloud);
				pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_color(display_cloud);
				pclVisualizer->updatePointCloud<pcl::PointXYZRGB>(display_cloud, rgb_color, "sample cloud");
				break;
			}
		}
	
		pclVisualizer->updateCamera();		
		pclVisualizer->spinOnce();
		boost::this_thread::sleep (boost::posix_time::microseconds (1000));
	}
}

void Visualizer::updateCameraPosition(Leap::Vector position) {
	pclVisualizer->camera_.pos[0] = position.x;
	pclVisualizer->camera_.pos[1] = position.y;	
	pclVisualizer->camera_.pos[2] = position.z;	
}