#ifndef MEDMODELVIS_H
#define MEDMODELVIS_H

#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include "InputDriver.hpp"
#include "LeapListener.hpp"
#include "Visualizer.hpp"

#define FULL_FILE
#ifdef FULL_FILE
	#define DATA_FILE			"data/mrbrain_full.pcd"
	#define DATA_FILE_BINARY	"data/mrbrain_full.pcdb"
#else
	#define DATA_FILE			"data/mrbrain.pcd"
	#define DATA_FILE_BINARY	"data/mrbrain.pcdb"
#endif

class MedModelVis {
private:
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
	InputDriver *inputDriver;
	Visualizer *visualizer;
	LeapListener *leapListener;
	float fmap(float x, float in_min, float in_max, float out_min, float out_max);

public:
	MedModelVis();
	void run();
	void initData();
	void saveCloudToBinary();
};

#endif