#ifndef LEAP_LISTENER_H
#define LEAP_LISTENER_H

#include "InputDriver.hpp"
#include "Leap.h"

class LeapListener : public Leap::Listener {
private:
	InputDriver *buf;
	Leap::Controller controller;
	Leap::Vector prevPos;
public:
	LeapListener(InputDriver *buf_);
	void run();
    virtual void onInit(const Leap::Controller&);
    virtual void onConnect(const Leap::Controller&);
    virtual void onDisconnect(const Leap::Controller&);
    virtual void onExit(const Leap::Controller&);
    virtual void onFrame(const Leap::Controller&);
};

#endif