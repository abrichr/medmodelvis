#ifndef MOVEMENT_BUFFER_H
#define MOVEMENT_BUFFER_H

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/circular_buffer.hpp>
#include "Leap.h"

#define DEFAULT_BUFFER_SIZE				1
#define TIMEOUT_MILLIS					10
#define DEFAULT_INVERT_CAMERA			false
#define DISPLACEMENT_AMPLIFICATION		1
#define VELOCITY_AMPLIFICATION			0.2
#define DEFAULT_COORDINATE_SYSTEM		GEODESIC
#define DEFAULT_INPUT_MODE				POSITION
#define MIN_ALTITUDE					150
#define MAX_ALTITUDE					1000
#define ACTIVE_REGION_SIZE				150
#define DEFAULT_LATITUDE				0
#define DEFAULT_LONGITUDE				0
#define DEFAULT_ALTITUDE				500
#define USE_USER_ORIGIN					true

class InputDriver {
public:

	enum CoordinateSystem {
		CARTESIAN,
		GEODESIC
	};
	enum InputMode {
		POSITION,
		VELOCITY,
		GESTURE
	};
	enum InputTarget {
		CAMERA_POSITION,
		CAMERA_ORIENTATION,
		CAMERA_FOCUS,
		PLANE_POSITION,
		PLANE_ORIENTATION,
		SLICE_GESTURE
	};

	struct InputMessage {
		InputTarget target;
		Leap::Vector data;
	};
	struct GeodesicPosition {
		GeodesicPosition() :
			latitude(0),
			longitude(0),
			altitude(0)
		{}
		GeodesicPosition(double lat, double lon, double alt) :
			latitude(lat),
			longitude(lon),
			altitude(alt)
		{}
		double latitude;	// -90: south, 90: north
		double longitude;	// -180: west, 180: east
		double altitude;
	};
	InputDriver(
		int bufferSize_ = DEFAULT_BUFFER_SIZE,
		CoordinateSystem coordinateSystem_ = DEFAULT_COORDINATE_SYSTEM,
		InputMode inputMode_ = DEFAULT_INPUT_MODE
	);
	bool getInput(InputMessage &input);
	bool putPointables(Leap::PointableList data);
	bool putGestures(Leap::GestureList data);
	bool hasData();
	bool hasRoom();
	int getSize();
	int getCapacity();
	Leap::Vector getPosition();
	void toggleCoordinateSystem();
	void toggleInvertCamera();
	void printDebugInfo();
	void toggleInputMode();
	void resetPosition();
	void setDefaultPosition(
		double a,
		double b,
		double c,
		CoordinateSystem coordinateSystem
	);
	void setDefaultPosition(Leap::Vector cart);
	void setDefaultPosition(GeodesicPosition geo);

private:

	int bufferSize;
	boost::mutex mutex;
	boost::condition_variable buffEmpty;
	boost::condition_variable buffFull;
	boost::circular_buffer<Leap::PointableList> pointableList;
	boost::circular_buffer<Leap::GestureList> gestureList;
	CoordinateSystem coordinateSystem;
	Leap::Vector prevPos;
	Leap::Vector cartesianPosition;
	GeodesicPosition geodesicPosition;
	Leap::Vector origin;
	bool invertCamera;
	InputMode inputMode;
	Leap::Vector defaultPosition;
	bool originWasInitialized;
	bool getInputMessage(InputMessage &input);
	void syncCoordinates();
	GeodesicPosition cartToGeo(Leap::Vector cart);
	Leap::Vector geoToCart(GeodesicPosition geo);
	std::vector<Leap::Pointable> getValidPointables(Leap::PointableList pointables);
	std::vector<Leap::Gesture> getValidGestures(Leap::GestureList gestures);
	void enforceBoundaries();
	void initOrigin(std::vector<Leap::Pointable> pointables);
	void sortPointables(std::vector<Leap::Pointable> pointables);
	static bool isFirstLeftOf(Leap::Pointable a, Leap::Pointable b);
	static void printFrameInfo(Leap::Frame frame);
};

#endif