#include <pcl/io/pcd_io.h>
#include <pcl/common/centroid.h>
#include <boost/thread/thread.hpp>
#include "MedModelVis.hpp"
#include <iostream>
#include <cstdio>

MedModelVis::MedModelVis() {
}

void MedModelVis::initData() {
	std::cout << "Loading data..." << std::endl;
	pcl::PointCloud<pcl::PointXYZI>::Ptr tempCloud = pcl::PointCloud<pcl::PointXYZI>::Ptr(new pcl::PointCloud<pcl::PointXYZI>);
	if (pcl::io::loadPCDFile<pcl::PointXYZI> (DATA_FILE_BINARY, *tempCloud) == -1) {
		PCL_ERROR ("Couldn't read file " DATA_FILE_BINARY "\n");
		getchar();
		exit(-1);
	}

	
	//std::cout << "Demeaning..." << std::endl;
	//Eigen::Vector4f centroid;
	//pcl::compute3DCentroid (*cloud, centroid);
	//pcl::demeanPointCloud(*cloud,centroid,*cloud);
	//
	//saveCloudToBinary();
	std::cout << "Converting to RGB..." << std::endl;
	cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
	cloud->resize(tempCloud->size());
	for (size_t i = 0; i < tempCloud->points.size(); i++) {
		cloud->points[i].x = tempCloud->points[i].x;
		cloud->points[i].y = tempCloud->points[i].y;
		cloud->points[i].z = tempCloud->points[i].z;
		cloud->points[i].r = cloud->points[i].g = cloud->points[i].b = tempCloud->points[i].intensity;
	}

	//std::cout << "Linear Scaling Itensity..." << std::endl;

	//float min = 0;
	//float max = 0;

	//float min_t = 0;
	//float max_t = 255;

	//for (size_t i = 0; i < cloud->points.size(); i++) {
	//	if (cloud->points[i].intensity < min) { min = cloud->points[i].intensity;}
	//	if (cloud->points[i].intensity > max) { max = cloud->points[i].intensity;}
	//}

	//std::cout << "min: " << min << " max: " << max << std::endl;

	//for (size_t i = 0; i < cloud->points.size(); i++) {

	//	cloud->points[i].intensity = fmap(cloud->points[i].intensity,min,max,min_t,max_t);
	//}
}

float MedModelVis::fmap(float x, float in_min, float in_max, float out_min, float out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void MedModelVis::run() {
	initData();

	inputDriver = new InputDriver();
	leapListener = new LeapListener(inputDriver);
	visualizer = new Visualizer(cloud, inputDriver);

	boost::thread thLeapListener(boost::bind(&LeapListener::run, leapListener));
	boost::thread thVisualizer(boost::bind(&Visualizer::run, visualizer));

	thLeapListener.join();
	thVisualizer.join();
}

void MedModelVis::saveCloudToBinary() {
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr tempCloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
	tempCloud->resize(cloud->size());
	for (size_t i = 0; i < tempCloud->points.size(); ++i) {
		tempCloud->points[i].x = cloud->points[i].x;
		tempCloud->points[i].y = cloud->points[i].y;
		tempCloud->points[i].z = cloud->points[i].z;
		//tempCloud->points[i].intensity = cloud->points[i].intensity;
	}

	pcl::io::savePCDFile<pcl::PointXYZRGB>(DATA_FILE_BINARY, *tempCloud, true);
}