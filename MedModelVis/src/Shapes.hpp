#ifndef SHAPES_H
#define SHAPES_H

#include <string>
#include <pcl/common/common_headers.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/visualization/pcl_visualizer.h>

class Shape
{

private:

protected:
	std::string id;
	bool visible;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> vis;
	Eigen::Affine3f pose;

public:
	Shape();
	Shape( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_);

	void hide();

	void updatePose(Eigen::Affine3f& transform);
	Eigen::Affine3f getPose();

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

class Sphere : public Shape
{

	pcl::PointXYZ center;

public:

	Sphere();
	Sphere( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_);
	void show();

	//EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

class Plane : public Shape
{

	pcl::ModelCoefficients coeffs;

public:
	Plane();
	Plane( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_);
	void show();

	//EIGEN_MAKE_ALIGNED_OPERATOR_NEW

};

#endif