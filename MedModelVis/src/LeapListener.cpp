#include "LeapListener.hpp"
#include <iostream>

LeapListener::LeapListener(InputDriver *buf_) :
	buf(buf_) {

}

void LeapListener::run() {
	controller.addListener(*this);
}

void LeapListener::onInit(const Leap::Controller& controller) {
  std::cout << "Initialized" << std::endl;
}

void LeapListener::onConnect(const Leap::Controller& controller) {
  std::cout << "Connected" << std::endl;
  controller.enableGesture(Leap::Gesture::TYPE_CIRCLE);
  controller.enableGesture(Leap::Gesture::TYPE_KEY_TAP);
  controller.enableGesture(Leap::Gesture::TYPE_SCREEN_TAP);
  controller.enableGesture(Leap::Gesture::TYPE_SWIPE);
}

void LeapListener::onDisconnect(const Leap::Controller& controller) {
  std::cout << "Disconnected" << std::endl;
}

void LeapListener::onExit(const Leap::Controller& controller) {
  std::cout << "Exited" << std::endl;
}

void LeapListener::onFrame(const Leap::Controller& controller) {
  const Leap::Frame frame = controller.frame();
  if (!frame.pointables().empty()) {
	  buf->putPointables(frame.pointables());
  }
  else if(!frame.gestures().empty())
  {
	  buf->putGestures(frame.gestures());
  }
}