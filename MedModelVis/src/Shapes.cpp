#include "Shapes.hpp"

#include <string>
#include <pcl/common/common_headers.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/visualization/pcl_visualizer.h>

void Shape::updatePose(Eigen::Affine3f& transform){
	pose = pose * transform;
	vis->updateShapePose(id,transform);
}

Eigen::Affine3f Shape::getPose(){
	return pose;
}

void Shape::hide(){
	if(visible){
		vis->removeShape(id,0);
		visible = false;
	}
}

void Sphere::show(){
	if(!visible){

		pose = Eigen::Affine3f();
		vis->addSphere(center, 3, 255, 0, 0, id, 0);
		vis->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION, pcl::visualization::PCL_VISUALIZER_REPRESENTATION_SURFACE,id, 0);
		visible = true;
	}
}

void Plane::show(){
	if(!visible){
		//set plane parameters (z-orthogonal)
		coeffs.values.clear();	
		coeffs.values.push_back(0.0);
		coeffs.values.push_back(0.0);
		coeffs.values.push_back(1.0);
		coeffs.values.push_back(0.0);

		pose = Eigen::Affine3f();
		vis->addPlane(coeffs, id, 0);
		visible = true;
	}
}

Shape::Shape( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_)
	: id(id_), vis(vis_), visible(false){
		pose = Eigen::Affine3f::Identity();
}

Sphere::Sphere( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_)
	: Shape( id_, vis_){}

Plane::Plane( std::string id_, boost::shared_ptr<pcl::visualization::PCLVisualizer> vis_)
	: Shape( id_, vis_){}

Shape::Shape(){}

Sphere::Sphere(){}

Plane::Plane(){}