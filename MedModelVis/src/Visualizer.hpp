#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include "InputDriver.hpp"

class Visualizer {
private:
	boost::shared_ptr<pcl::visualization::PCLVisualizer> pclVisualizer;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr display_cloud;
	InputDriver *inputDriver;
	float theta;
	float phi;
	float radius;

	void init();
	void updateCameraPosition(Leap::Vector position);
	void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &e);
public:
	Visualizer(
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_,
		InputDriver *inputDriver_
	);
	void run();
};

#endif